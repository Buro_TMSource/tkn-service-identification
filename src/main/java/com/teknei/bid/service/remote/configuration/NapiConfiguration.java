package com.teknei.bid.service.remote.configuration;

import com.teknei.bid.controller.rest.crypto.Decrypt;
import feign.auth.BasicAuthRequestInterceptor;
import org.jolokia.util.Base64Util;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Configuration
public class NapiConfiguration {

    private String username;
    private String password;
    @Value("${tkn.identification.napi.secret}")
    private String secretName;
    @Autowired
    private Decrypt decrypt;
    private static final Logger log = LoggerFactory.getLogger(BasicCurpConfig.class);

    @PostConstruct
    private void init(){
        initSecret();
    }

    private void initSecret(){
        final String secretUri = "/run/secrets/"+secretName;
        String user = username;
        String pass = password;
        try {
            String content = new String(Files.readAllBytes(Paths.get(secretUri)));
            JSONObject jsonObject = new JSONObject(content);
            if (content == null || content.isEmpty()) {
                log.error("No secret supplied, leaving default");
            } else {
                user = jsonObject.optString("username", username);
                pass = jsonObject.optString("password", password);
                user = decrypt(user);
                pass = decrypt(pass);
            }
        } catch (IOException e) {
            log.error("No secret supplied, leaving default");
            user = "buroidentidad";
            pass = "_yh54RSm21";
        }
        username = user;
        password = pass;
    }

    @Bean
    public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
        return new BasicAuthRequestInterceptor("buroidentidad", "_yh54RSm21");
    }

    private String decrypt(String source) {
        try {
            String decrypted = decrypt.decrypt(source);
            if(decrypted == null){
                return source;
            }
            return new String(Base64Util.decode(decrypted));
        } catch (Exception e) {
            log.warn("No ciphered content, returning clear");
            return source;
        }
    }
}
