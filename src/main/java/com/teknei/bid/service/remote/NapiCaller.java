package com.teknei.bid.service.remote;

import com.teknei.bid.service.remote.configuration.NapiConfiguration;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient(name = "napiCaller", url = "https://www.napi.buroidentidad.com", configuration = NapiConfiguration.class)
public interface NapiCaller {

    @RequestMapping(value = "/ocr/obtener_datos", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getData(@RequestBody String jsonBody);
}
