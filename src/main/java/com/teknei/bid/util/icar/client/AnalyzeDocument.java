
package com.teknei.bid.util.icar.client;

import javax.xml.bind.annotation.*;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="aUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aPwd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aDsIn" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;any namespace='http://IdCloud.iCarVision/WS/'/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "aUser",
    "aPwd",
    "aDsIn"
})
@XmlRootElement(name = "AnalyzeDocument")
public class AnalyzeDocument {

    protected String aUser;
    protected String aPwd;
    protected ADsIn aDsIn;

    /**
     * Obtiene el valor de la propiedad aUser.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAUser() {
        return aUser;
    }

    /**
     * Define el valor de la propiedad aUser.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAUser(String value) {
        this.aUser = value;
    }

    /**
     * Obtiene el valor de la propiedad aPwd.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAPwd() {
        return aPwd;
    }

    /**
     * Define el valor de la propiedad aPwd.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAPwd(String value) {
        this.aPwd = value;
    }

    /**
     * Obtiene el valor de la propiedad aDsIn.
     *
     * @return
     *     possible object is
     *     {@link ADsIn }
     *
     */
    public ADsIn getADsIn() {
        return aDsIn;
    }

    /**
     * Define el valor de la propiedad aDsIn.
     *
     * @param value
     *     allowed object is
     *     {@link ADsIn }
     *
     */
    public void setADsIn(ADsIn value) {
        this.aDsIn = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;any namespace='http://IdCloud.iCarVision/WS/'/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "any"
    })
    public static class ADsIn {

        @XmlAnyElement(lax = true)
        protected Object any;

        /**
         * Obtiene el valor de la propiedad any.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getAny() {
            return any;
        }

        /**
         * Define el valor de la propiedad any.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setAny(Object value) {
            this.any = value;
        }

    }

}
