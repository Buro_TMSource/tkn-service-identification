
package com.teknei.bid.util.icar.client;

import javax.xml.bind.annotation.*;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AnalyzeDocumentV2ExResult" type="{http://IdCloud.iCarVision/WS/}DocumentCheckOutV2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "analyzeDocumentV2ExResult"
})
@XmlRootElement(name = "AnalyzeDocumentV2ExResponse")
public class AnalyzeDocumentV2ExResponse {

    @XmlElement(name = "AnalyzeDocumentV2ExResult")
    protected DocumentCheckOutV2 analyzeDocumentV2ExResult;

    /**
     * Obtiene el valor de la propiedad analyzeDocumentV2ExResult.
     * 
     * @return
     *     possible object is
     *     {@link DocumentCheckOutV2 }
     *     
     */
    public DocumentCheckOutV2 getAnalyzeDocumentV2ExResult() {
        return analyzeDocumentV2ExResult;
    }

    /**
     * Define el valor de la propiedad analyzeDocumentV2ExResult.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentCheckOutV2 }
     *     
     */
    public void setAnalyzeDocumentV2ExResult(DocumentCheckOutV2 value) {
        this.analyzeDocumentV2ExResult = value;
    }

}
