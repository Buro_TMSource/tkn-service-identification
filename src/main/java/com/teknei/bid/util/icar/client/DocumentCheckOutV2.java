
package com.teknei.bid.util.icar.client;

import javax.xml.bind.annotation.*;


/**
 * <p>Clase Java para DocumentCheckOutV2 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocumentCheckOutV2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Fields" type="{http://IdCloud.iCarVision/WS/}ArrayOfField" minOccurs="0"/>
 *         &lt;element name="Messages" type="{http://IdCloud.iCarVision/WS/}ArrayOfMessage" minOccurs="0"/>
 *         &lt;element name="DocumentIdForMerging" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Image1cut" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="Image2cut" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="ImagePhoto" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="ImageSignature" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="ImageFingerPrint" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="Result" type="{http://IdCloud.iCarVision/WS/}DocumentResultPrivateV2"/>
 *         &lt;element name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Side" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentCheckOutV2", propOrder = {
    "fields",
    "messages",
    "documentIdForMerging",
    "reference",
    "image1Cut",
    "image2Cut",
    "imagePhoto",
    "imageSignature",
    "imageFingerPrint",
    "result",
    "warning",
    "side"
})
public class DocumentCheckOutV2 {

    @XmlElement(name = "Fields")
    protected ArrayOfField fields;
    @XmlElement(name = "Messages")
    protected ArrayOfMessage messages;
    @XmlElement(name = "DocumentIdForMerging")
    protected String documentIdForMerging;
    @XmlElement(name = "Reference")
    protected String reference;
    @XmlElement(name = "Image1cut")
    protected byte[] image1Cut;
    @XmlElement(name = "Image2cut")
    protected byte[] image2Cut;
    @XmlElement(name = "ImagePhoto")
    protected byte[] imagePhoto;
    @XmlElement(name = "ImageSignature")
    protected byte[] imageSignature;
    @XmlElement(name = "ImageFingerPrint")
    protected byte[] imageFingerPrint;
    @XmlElement(name = "Result", required = true)
    @XmlSchemaType(name = "string")
    protected DocumentResultPrivateV2 result;
    @XmlElement(name = "Warning")
    protected String warning;
    @XmlElement(name = "Side")
    protected String side;

    /**
     * Obtiene el valor de la propiedad fields.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfField }
     *     
     */
    public ArrayOfField getFields() {
        return fields;
    }

    /**
     * Define el valor de la propiedad fields.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfField }
     *     
     */
    public void setFields(ArrayOfField value) {
        this.fields = value;
    }

    /**
     * Obtiene el valor de la propiedad messages.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMessage }
     *     
     */
    public ArrayOfMessage getMessages() {
        return messages;
    }

    /**
     * Define el valor de la propiedad messages.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMessage }
     *     
     */
    public void setMessages(ArrayOfMessage value) {
        this.messages = value;
    }

    /**
     * Obtiene el valor de la propiedad documentIdForMerging.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentIdForMerging() {
        return documentIdForMerging;
    }

    /**
     * Define el valor de la propiedad documentIdForMerging.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentIdForMerging(String value) {
        this.documentIdForMerging = value;
    }

    /**
     * Obtiene el valor de la propiedad reference.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReference() {
        return reference;
    }

    /**
     * Define el valor de la propiedad reference.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReference(String value) {
        this.reference = value;
    }

    /**
     * Obtiene el valor de la propiedad image1Cut.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getImage1Cut() {
        return image1Cut;
    }

    /**
     * Define el valor de la propiedad image1Cut.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setImage1Cut(byte[] value) {
        this.image1Cut = value;
    }

    /**
     * Obtiene el valor de la propiedad image2Cut.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getImage2Cut() {
        return image2Cut;
    }

    /**
     * Define el valor de la propiedad image2Cut.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setImage2Cut(byte[] value) {
        this.image2Cut = value;
    }

    /**
     * Obtiene el valor de la propiedad imagePhoto.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getImagePhoto() {
        return imagePhoto;
    }

    /**
     * Define el valor de la propiedad imagePhoto.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setImagePhoto(byte[] value) {
        this.imagePhoto = value;
    }

    /**
     * Obtiene el valor de la propiedad imageSignature.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getImageSignature() {
        return imageSignature;
    }

    /**
     * Define el valor de la propiedad imageSignature.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setImageSignature(byte[] value) {
        this.imageSignature = value;
    }

    /**
     * Obtiene el valor de la propiedad imageFingerPrint.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getImageFingerPrint() {
        return imageFingerPrint;
    }

    /**
     * Define el valor de la propiedad imageFingerPrint.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setImageFingerPrint(byte[] value) {
        this.imageFingerPrint = value;
    }

    /**
     * Obtiene el valor de la propiedad result.
     * 
     * @return
     *     possible object is
     *     {@link DocumentResultPrivateV2 }
     *     
     */
    public DocumentResultPrivateV2 getResult() {
        return result;
    }

    /**
     * Define el valor de la propiedad result.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentResultPrivateV2 }
     *     
     */
    public void setResult(DocumentResultPrivateV2 value) {
        this.result = value;
    }

    /**
     * Obtiene el valor de la propiedad warning.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarning() {
        return warning;
    }

    /**
     * Define el valor de la propiedad warning.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarning(String value) {
        this.warning = value;
    }

    /**
     * Obtiene el valor de la propiedad side.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSide() {
        return side;
    }

    /**
     * Define el valor de la propiedad side.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSide(String value) {
        this.side = value;
    }

}
