package com.teknei.bid.util.icar;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Amaro on 14/08/2017.
 */
public class IcarDataSingleton implements Serializable {

    private Map<String, String> informationMap;
    private Map<String, Object> imageMap;

    private IcarDataSingleton() {
        informationMap = new HashMap<>();
        imageMap = new HashMap<>();
    }

    private static final IcarDataSingleton instance = new IcarDataSingleton();

    public static IcarDataSingleton getInstance() {
        return instance;
    }

    public Map<String, String> getInformationMap() {
        return informationMap;
    }

    public Map<String, Object> getImageMap(){
        return imageMap;
    }

}
