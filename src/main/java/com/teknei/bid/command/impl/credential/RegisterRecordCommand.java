package com.teknei.bid.command.impl.credential;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.persistence.entities.BidClieCurp;
import com.teknei.bid.persistence.entities.BidClieRegProc;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.persistence.repository.BidRegProcRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class RegisterRecordCommand implements Command {

    @Autowired
    private BidCurpRepository bidCurpRepository;
    @Autowired
    private BidRegProcRepository bidRegProcRepository;
    private static final Logger log = LoggerFactory.getLogger(RegisterRecordCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
        try{
            addRegisterRecord(request.getId(), true);
        }catch(Exception e){
            log.error("Error in registerRecordCommand for: {} with message: {}", request.getId(), e.getMessage());
        }
        return null;
    }

    private void addRegisterRecord(Long operationId, boolean created) {
        BidClieCurp clieCurp = bidCurpRepository.findTopByIdClie(operationId);
        BidClieRegProc regProc = bidRegProcRepository.findTopByCurp(clieCurp.getCurp());
        regProc.setRegIne(created);
        regProc.setFchRegIne(new Timestamp(System.currentTimeMillis()));
        bidRegProcRepository.save(regProc);
    }
}