package com.teknei.bid.command.impl.credential;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PassportParser {

    private static final Logger log = LoggerFactory.getLogger(PassportParser.class);

    public JSONObject parsePassport(JSONObject source){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", "OK");
        jsonObject.put("scanId", source.getString("scanId"));
        jsonObject.put("passport", "true");
        JSONObject document = new JSONObject();
        String passportNumber = getPassportNumber(source.getString("answer0"));
        String passportType = getPassportType(source.getString("answer0"));
        String passportCountryCode = getPassportExpeditionCountryCode(source.getString("answer0"));
        String passportSurnemas = getSurnames(source.getString("answer0"));
        String passportNames = getNames(source.getString("answer0"));
        String passportBirthdate = getBirth(source.getString("answer0"));
        String passportGender = getGender(source.getString("answer0"));
        String passportBirtplace = getBirthPlace(source.getString("answer0"));
        String passportDateExp = getDateExp(source.getString("answer0"));
        String passportDateCaducity = getDateCaducity(source.getString("answer0"));
        /*
        * document.put("CRC_SECTION", "NA");
            document.put("MRZ", "NA");
            document.put("MRZ", "NA");
            BidClie bidClie = bidClieRepository.findOne(request.getId());
            document.put("surname", bidClie.getApePate() + " " + bidClie.getApeMate());
            document.put("firstSurname", bidClie.getApePate());
            document.put("secondSurname", bidClie.getApeMate());
            document.put("name", bidClie.getNomClie());
            document.put("address", "NA");
            document.put("curp", "NA");
            document.put("dateOfBirth", "NA");
            jsonObject.put("document", document);**/
        document.put("CRC_SECTION", passportNumber);
        document.put("MRZ", passportNumber);
        document.put("passportType", passportType);
        document.put("passportCountryCode", passportCountryCode);
        document.put("surname", passportSurnemas);
        document.put("firstSurname", passportSurnemas);
        String[] surnames = passportSurnemas.split("\\s");
        if(surnames.length > 1){
            document.put("secondSurname", surnames[1]);
            document.put("firstSurname", surnames[0]);
        }
        document.put("name", passportNames);
        document.put("address", "NA");
        document.put("dateOfBirth", passportBirthdate);
        document.put("passportGender", passportGender);
        document.put("passportBirthplace", passportBirtplace);
        document.put("passportDateExp", passportDateExp);
        document.put("passportDateCaducity", passportDateCaducity);
        jsonObject.put("document", document);
        return jsonObject;

    }

    private String getDateCaducity(String text) {
        String regex = "\\d{2}\\s\\d{2}\\s\\d{4}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        List<String> matches = new ArrayList<>();
        while (matcher.find()) {
            String content = text.substring(matcher.start(), matcher.end());
            matches.add(content);
        }
        if (matches.isEmpty()) {
            return "01 01 1970";
        }
        if (matches.size() >= 3) {
            log.info("Found: {}", matches.get(2));
            return matches.get(2);
        }
        return "01 01 1970";
    }

    private String getDateExp(String text) {
        String regex = "\\d{2}\\s\\d{2}\\s\\d{4}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        List<String> matches = new ArrayList<>();
        while (matcher.find()) {
            String content = text.substring(matcher.start(), matcher.end());
            matches.add(content);
        }
        if (matches.isEmpty()) {
            return "01 01 1970";
        }
        if (matches.size() >= 2) {
            log.info("Found: {}", matches.get(1));
            return matches.get(1);
        }
        return "01 01 1970";
    }

    private String getBirthPlace(String text) {
        String token = "Fecha de ex";
        if (text.contains(token)) {
            text = text.substring(text.indexOf(token) - 50, text.indexOf(token));
            String regex = "[A-Z]{3,15}(,\\s...|(\\s[A-Z]{3,15}|\\s))";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(text);
            if (matcher.find()) {
                String content = text.substring(matcher.start(), matcher.end());
                log.info("Place of birth: {}", content);
                return content;
            }
            return "NO RECONOCIDO";
        }
        return "NO RECONOCIDO";
    }

    private String getGender(String text) {
        return "M";
    }

    private String getBirth(String text) {
        String regex = "\\d{2}\\s\\d{2}\\s\\d{4}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String precontent = text.substring(matcher.start(), matcher.end());
            precontent = precontent.replace("\\\\n", "").replace("\\n", "").trim();
            log.info("Precontent1: {}", precontent);
            return precontent;
        }
        return "01 01 1970";
    }

    private String getNames(String text) {
        String regex = "\\\\n[A-ZÑ]{4,20}\\\\n";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String precontent = text.substring(matcher.start(), matcher.end());
            precontent = precontent.replace("\\\\n", "").replace("\\n", "").trim();
            log.info("Precontent1: {}", precontent);
            return precontent;
        }
        return "NO LEGIBLE";
    }

    private String getSurnames(String text) {
        text = text.substring(50);
        String regex = "[A-ZÑ]{3,20}\\s[A-ZÑ]{3,20}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String precontent = text.substring(matcher.start(), matcher.end());
            log.info("Precontent1: {}", precontent);
            return precontent;
        }
        return "NO LEGIBLE";
    }

    private String getPassportExpeditionCountryCode(String text) {
        String regex = "\\\\n[A-Z]{3,5}\\\\n";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String precontent = text.substring(matcher.start(), matcher.end());
            precontent = precontent.replace("\\\\n", "").replace("\\n", "").trim();
            log.info("Country expedition found: {}", precontent);
            return precontent;
        }
        return "MEX";
    }

    private String getPassportNumber(String text) {
        text = text.replace(" ", "");
        String regex = "\\w{4,20}MEX\\d{3}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String preContent = text.substring(matcher.start(), matcher.end());
            if (preContent.startsWith("n")) {
                preContent = preContent.substring(1, preContent.length());
            } else {
                int index = preContent.indexOf("\\n");
                if (index != -1) {
                    preContent = preContent.substring(index, preContent.length());
                }
            }
            preContent = preContent.trim();
            log.info("Precontent: {}", preContent);
            return preContent;
        }
        return "NA";
    }

    private String getPassportType(String text) {
        String regex = "\\\\n[a-zA-Z]&lt;[A-Z]{2,3}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String type = text.substring(matcher.start(), matcher.end());
            type = type.substring(2, 3);
            log.info("Type found:{}", type);
            return type;
        }
        return "P";
    }
}