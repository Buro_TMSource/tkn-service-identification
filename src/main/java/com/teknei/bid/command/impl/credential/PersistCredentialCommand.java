package com.teknei.bid.command.impl.credential;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.dto.AddressIneDTO;
import com.teknei.bid.persistence.entities.*;
import com.teknei.bid.persistence.repository.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

@Component
public class PersistCredentialCommand implements Command {

    private static final Logger log = LoggerFactory.getLogger(PersistCredentialCommand.class);

    @Autowired
    private BidScanRepository bidScanRepository;
    @Autowired
    private BidTasRepository bidTasRepository;
    @Autowired
    private BidRegProcRepository bidRegProcRepository;
    @Autowired
    private BidIfeRepository bidIfeRepository;
    @Autowired
    private BidIfeRawRepository bidIfeRawRepository;
    @Autowired
    private BidCliePasaRepository bidCliePasaRepository;
    @Value("${tkn.identification.ocr.active}")
    private Boolean activeOcr;

    @Override
    public CommandResponse execute(CommandRequest request) {
        CommandResponse response = new CommandResponse();
        try {
            JSONObject data = new JSONObject(request.getData());
            data.put("id", request.getId());
            addDBInfo(request.getId(), request.getScanId(), data.toString(), request.getDocumentId(), request.getUsername());
            response.setStatus(Status.CREDENTIALS_DB_OK);
            response.setDesc("OK");
        } catch (Exception e) {
            log.error("Error in PersistCredentialCommand : {}", e.getMessage());
            response.setStatus(Status.CREDENTIALS_DB_ERROR);
            response.setDesc(e.getMessage());
            response.setId(request.getId());
        }
        return response;
    }

    private void addDBInfo(Long customerId, String scanId, String dataResponse, String tasId, String username) {
        addTasInfo(customerId, tasId, username);
        addScanInfo(customerId, scanId, dataResponse, username);
        addIdentificationData(dataResponse, username);
    }

    private void savePassport(JSONObject generalDocument, String username) {
        String formatDate = "dd MM yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(formatDate);
        BidCliePasa passport = new BidCliePasa();
        passport.setIdClie(generalDocument.getLong("id"));
        passport.setIdPasa(generalDocument.getLong("id"));
        JSONObject internalDocument = generalDocument.getJSONObject("document");
        passport.setApel(internalDocument.getString("surname"));
        passport.setNomb(internalDocument.getString("name"));
        try {
            passport.setFchCadu(new Timestamp(sdf.parse(internalDocument.getString("passportDateCaducity")).getTime()));
        } catch (Exception e) {
            log.error("Error set end date");
        }
        try {
            passport.setFchExpe(new Timestamp(sdf.parse(internalDocument.getString("passportDateExp")).getTime()));
        } catch (Exception e) {
            log.error("Error set exp date");
        }
        try {
            passport.setFchNac(new Timestamp(sdf.parse(internalDocument.getString("dateOfBirth")).getTime()));
        } catch (ParseException e) {
            log.error("Error set birth date");
        }
        passport.setFchCrea(new Timestamp(System.currentTimeMillis()));
        passport.setIdEsta(1);
        passport.setIdTipo(3);
        passport.setUsrCrea(username);
        passport.setUsrOpeCrea(username);
        passport.setTipo(internalDocument.getString("passportType"));
        passport.setLugNac(internalDocument.getString("passportBirthplace"));
        passport.setNumPasa(internalDocument.getString("CRC_SECTION"));
        bidCliePasaRepository.save(passport);
    }


    private void addIdentificationData(String icarResponse, String username) {
        if (!activeOcr) {
            return;
        }
        JSONObject generalDocument = new JSONObject(icarResponse);
        if (generalDocument.has("passport")) {
            BidCliePasaPK pasaPK = new BidCliePasaPK();
            pasaPK.setIdClie(generalDocument.getLong("id"));
            pasaPK.setIdPasa(generalDocument.getLong("id"));
            BidCliePasa passport = bidCliePasaRepository.findOne(pasaPK);
            if (passport == null) {
                savePassport(generalDocument, username);
            } else {
                //updatePassport(generalDocument, passport);
            }
            return;
        }
        JSONObject internalDocument = generalDocument.getJSONObject("document");
        BidClieIfeInePK ifeInePK = new BidClieIfeInePK();
        ifeInePK.setIdClie(generalDocument.getLong("id"));
        ifeInePK.setIdIfe(generalDocument.getLong("id"));
        BidClieIfeIne retrieved = bidIfeRepository.findOne(ifeInePK);
        if (retrieved == null) {
            saveIneInfo(generalDocument, internalDocument, username);
        } else {
            updateIneInfo(generalDocument, internalDocument, retrieved, username);
        }
        BidClieIfeIneBita ineRaw = new BidClieIfeIneBita();
        ineRaw.setIdIfe(generalDocument.getLong("id"));
        ineRaw.setIdClie(generalDocument.getLong("id"));
        ineRaw.setRespUno(generalDocument.toString());
        ineRaw.setRespDos(internalDocument.toString());
        ineRaw.setUsrCrea(username);
        ineRaw.setUsrOpeCrea(username);
        ineRaw.setFchCrea(new Timestamp(System.currentTimeMillis()));
        ineRaw.setIdEsta(1);
        ineRaw.setIdTipo(3);
        bidIfeRawRepository.save(ineRaw);
    }

    private void updateIneInfo(JSONObject generalDocument, JSONObject internalDocument, BidClieIfeIne ineRequest, String username) {
        ineRequest.setApeMate(internalDocument.optString("secondSurname", ineRequest.getApeMate()));
        ineRequest.setApePate(internalDocument.optString("firstSurname", ineRequest.getApePate()));
        String addressRequest = internalDocument.optString("address", null);
        if (addressRequest != null && !addressRequest.isEmpty()) {
            AddressIneDTO addressIneDTO = parseDetail(addressRequest);
            ineRequest.setCall(addressIneDTO.getCall());
            ineRequest.setCol(addressIneDTO.getCol());
            //ineRequest.setMuni(addressIneDTO.getMuni());
            //ineRequest.setEsta(addressIneDTO.getEsta());
            if (addressIneDTO.getCp() != null && addressIneDTO.getCp().length() <= 5) {
                ineRequest.setCp(addressIneDTO.getCp());
            }
        }
        ineRequest.setClavElec(internalDocument.optString("claveElector", ineRequest.getClavElec()));
        ineRequest.setEsta(internalDocument.optString("issuingState", ineRequest.getEsta()));
        ineRequest.setOcr(internalDocument.optString("CRC_SECTION", ineRequest.getOcr()));
        ineRequest.setNomb(internalDocument.optString("name", ineRequest.getNomb()));
        ineRequest.setMrz(internalDocument.optString("MRZ", ineRequest.getMrz()));
        ineRequest.setUsrModi(username);
        ineRequest.setFchModi(new Timestamp(System.currentTimeMillis()));
        ineRequest.setCurp(internalDocument.optString("curp", ineRequest.getCurp()));
        ineRequest.setUsrOpeModi(username);
        bidIfeRepository.save(ineRequest);
    }

    private void saveIneInfo(JSONObject generalDocument, JSONObject internalDocument, String username) {
        BidClieIfeIne ineRequest = new BidClieIfeIne();
        ineRequest.setApeMate(internalDocument.optString("secondSurname", null));
        ineRequest.setApePate(internalDocument.optString("firstSurname", null));
        String addressRequest = internalDocument.optString("address", null);
        if (addressRequest != null && !addressRequest.isEmpty()) {
            AddressIneDTO addressIneDTO = parseDetail(addressRequest);
            ineRequest.setCall(addressIneDTO.getCall());
            ineRequest.setCol(addressIneDTO.getCol());
            //ineRequest.setMuni(addressIneDTO.getMuni());
            //ineRequest.setEsta(addressIneDTO.getEsta());
            if (addressIneDTO.getCp() != null && addressIneDTO.getCp().length() <= 5) {
                ineRequest.setCp(addressIneDTO.getCp());
            }
        }
        ineRequest.setCurp(internalDocument.optString("curp", null));
        ineRequest.setClavElec(internalDocument.optString("claveElector", null));
        ineRequest.setEsta(internalDocument.optString("issuingState", null));
        ineRequest.setOcr(internalDocument.optString("CRC_SECTION", null));
        ineRequest.setNomb(internalDocument.optString("name", null));
        ineRequest.setMrz(internalDocument.optString("MRZ", null));
        ineRequest.setFchCrea(new Timestamp(System.currentTimeMillis()));
        ineRequest.setIdEsta(1);
        ineRequest.setIdTipo(3);
        ineRequest.setUsrCrea(username);
        ineRequest.setUsrOpeCrea(username);
        ineRequest.setIdIfe(generalDocument.getLong("id"));
        ineRequest.setIdClie(generalDocument.getLong("id"));
        bidIfeRepository.save(ineRequest);
    }

    private AddressIneDTO parseDetail(String address) {
        address = address.replace("\n", "-");
        String[] list = address.split("-");
        String[] properties = {"call", "col", "muni"};
        JSONObject json = new JSONObject();
        for (int i = 0; i < list.length; i++) {
            if (i >= properties.length) {
                continue;//break
            }
            // Extrae el estado del campo de municipio
            String[] location = list[i].split(",");
            if (i == Arrays.asList(properties).indexOf("muni") && location.length > 1) {
                json.put(properties[i], location[0].trim());
                json.put("esta", location[1].trim().toUpperCase().replaceAll("([^A-Z\\s])", ""));
                continue;
            }
            // Extrae el código postal del campo colonia
            try {
                String zipCode = list[i].trim().substring(list[i].trim().length() - 5);
                if (i == Arrays.asList(properties).indexOf("col") && zipCode.matches("\\d{5}")) {
                    json.put(properties[i], list[i].trim().substring(0, list[i].trim().length() - 5));
                    json.put("cp", zipCode.trim());
                    continue;
                }
            } catch (Exception e) {
                log.error("Error extracting field from address in credential with message: {} continue with parsing", e.getMessage());
            }
            json.put(properties[i], list[i].trim());
        }
        AddressIneDTO ineDTO = new AddressIneDTO();
        if (json.has("call")) {
            ineDTO.setCall(json.getString("call"));
        }
        if (json.has("col")) {
            ineDTO.setCol(json.getString("col"));
        }
        if (json.has("muni")) {
            ineDTO.setMuni(json.getString("muni"));
        }
        if (json.has("esta")) {
            ineDTO.setEsta(json.getString("esta"));
        }
        if (json.has("cp")) {
            ineDTO.setCp(json.getString("cp"));
        }
        return ineDTO;
    }


    private void addTasInfo(Long customerId, String tasId, String username) {
        BidClieTas bidTas = new BidClieTas();
        bidTas.setUsrCrea(username);
        bidTas.setIdTas(tasId);
        bidTas.setIdClie(customerId);
        bidTas.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidTas.setUsrOpeCrea(username);
        bidTas.setIdEsta(1);
        bidTas.setIdTipo(3);
        bidTasRepository.save(bidTas);
    }

    private void addScanInfo(Long customerId, String scanId, String dataResponse, String username) {
        BidScan bidScan = bidScanRepository.findByIdRegi(customerId);
        if (bidScan == null) {
            bidScan = new BidScan();
            bidScan.setUsrCrea(username);
            bidScan.setUsrOpeCrea(username);
            bidScan.setFchCrea(new Timestamp(System.currentTimeMillis()));
            bidScan.setIdRegi(customerId);
            bidScan.setScanId(scanId);
        } else {
            bidScan.setUsrModi(username);
            bidScan.setUsrModi(username);
            bidScan.setFchModi(new Timestamp(System.currentTimeMillis()));
        }
        bidScan.setData(dataResponse);
        bidScanRepository.save(bidScan);
    }

}