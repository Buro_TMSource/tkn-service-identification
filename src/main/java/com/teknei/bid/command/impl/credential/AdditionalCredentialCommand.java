package com.teknei.bid.command.impl.credential;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.entities.BidScan;
import com.teknei.bid.persistence.repository.BidClieRegEstaRepository;
import com.teknei.bid.persistence.repository.BidEstaProcRepository;
import com.teknei.bid.persistence.repository.BidScanRepository;
import com.teknei.bid.persistence.repository.BidTasRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class AdditionalCredentialCommand implements Command {

    @Autowired
    @Qualifier(value = "parseCredentialVisionCommand")
    private Command parseCredentialCommand;
    @Autowired
    @Qualifier(value = "parseCredentialCommand")
    private Command parseCredentialCommandIcar;
    @Autowired
    @Qualifier(value = "persistCredentialCommand")
    private Command persistCredentialCommand;
    @Autowired
    @Qualifier(value = "storeTasAdditionalCredentialCommand")
    private Command storeTasAdditionalCredentialCommand;
    @Autowired
    @Qualifier(value = "statusCommand")
    private Command statusCommand;
    @Autowired
    @Qualifier(value = "registerRecordCommand")
    private Command registerRecordCommand;
    @Autowired
    private BidClieRegEstaRepository regEstaRepository;

    @Value("${tkn.identification.ocr.active}")
    private Boolean activeOcr;
    @Value("${tkn.identification.tas.active}")
    private Boolean activeTas;
    @Value("${tkn.identification.persist.active}")
    private Boolean activePersistence;
    @Value("${tkn.identification.ocr.provider}")
    private String ocrProvider;
    @Autowired
    private BidTasRepository bidTasRepository;
    @Autowired
    private BidScanRepository bidScanRepository;
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    private static final String ESTA_PROC = "CAP-2CRE";
    private static final Logger log = LoggerFactory.getLogger(AdditionalCredentialCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
        //registerRecordCommand.execute(request);
        String documentId = "";
        String scanId = "";
        CommandResponse response = null;
        CommandRequest tasRequest = new CommandRequest();
        tasRequest.setData(bidTasRepository.findByIdClie(request.getId()).getIdTas());
        tasRequest.setId(request.getId());
        BidScan scanEnt = bidScanRepository.findByIdRegi(request.getId());
        if (scanEnt != null) {
            scanId = scanEnt.getScanId();
        }
        tasRequest.setScanId(scanId);
        tasRequest.setFileContent(request.getFileContent());
        CommandResponse tasResponse = storeTasAdditionalCredentialCommand.execute(tasRequest);
        if (!tasResponse.getStatus().equals(Status.CREDENTIALS_TAS_OK)) {
            saveStatus(request.getId(), Status.CREDENTIALS_TAS_ERROR, request.getUsername());
            return tasResponse;
        }
        response = tasResponse;
        documentId = tasResponse.getDocumentId();
        scanId = tasRequest.getScanId();
        saveStatus(request.getId(), Status.CREDENTIALS_TAS_OK,request.getUsername());
        response.setStatus(Status.CREDENTIALS_OK);
        updateStatus(request.getId(), request.getUsername());
        /*
        if (activePersistence) {
            CommandRequest dbRequest = new CommandRequest();
            dbRequest.setId(request.getId());
            dbRequest.setData(response.getDesc());
            dbRequest.setDocumentId(documentId);
            dbRequest.setScanId(response.getScanId());
            CommandResponse dbResponse = persistCredentialCommand.execute(dbRequest);
            if (dbResponse.getStatus().equals(Status.CREDENTIALS_DB_OK)) {
                saveStatus(request.getId(), Status.CREDENTIALS_DB_OK);
                saveStatus(request.getId(), Status.CREDENTIALS_OK);
                response.setStatus(Status.CREDENTIALS_OK);
                response.setDesc(response.getDesc());
                response.setScanId(scanId);
                response.setDocumentId(documentId);
                updateStatus(request.getId());
            } else {
                saveStatus(request.getId(), Status.CREDENTIALS_DB_ERROR);
            }
        } else {
            saveStatus(request.getId(), Status.CREDENTIALS_DB_OK);
        }
        */
        return response;
    }


    /**
     * Persists the current status for the main process
     *
     * @param id
     * @param status
     * @return
     */
    private CommandResponse saveStatus(Long id, Status status, String username) {
        CommandRequest request = new CommandRequest();
        request.setId(id);
        request.setRequestStatus(status);
        request.setUsername(username);
        CommandResponse response = statusCommand.execute(request);
        return response;
    }

    private void updateStatus(Long idClient, String username) {
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(ESTA_PROC, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi(username);
            regEsta.setUsrOpeModi(username);
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }
}
