package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class IneDetailDTO implements Serializable {

    private String nomb;
    private String apePat;
    private String apeMat;
    private String call;
    private String noExt;
    private String noInt;
    private String col;
    private String cp;
    private String foli;
    private String clavElec;
    private String ocr;
    private String esta;
    private String muni;
    private String dist;
    private String loca;
    private String secc;
    private long vige;
    private String mrz;
    private String user;
    private String curp;

}