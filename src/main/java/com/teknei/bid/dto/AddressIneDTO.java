package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AddressIneDTO implements Serializable {

    private String call;
    private String col;
    private String cp;
    private String muni;
    private String esta;

}