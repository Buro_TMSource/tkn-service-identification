package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BasicRequestDTO implements Serializable {

    private String username;
    private Long idOperation;

}